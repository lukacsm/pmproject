import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class  Reader {

    public void readPersons() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("src/us-500.csv"));

        while (sc.hasNextLine()){
            String line = sc.nextLine();
            String[] parts = line.split(",");
            Person person = new Person(parts[0],
                    parts[1],
                    parts[4],
                    parts[10]);
        }
    }
}
